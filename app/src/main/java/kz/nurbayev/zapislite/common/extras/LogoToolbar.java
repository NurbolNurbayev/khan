package kz.nurbayev.zapislite.common.extras;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.TextView;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;

import kz.nurbayev.zapislite.R;


public class LogoToolbar extends Toolbar {

    private TextView titleTv;

    public LogoToolbar(Context context) {
        super(context);
        setup(context, null, 0);
    }

    public LogoToolbar(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        setup(context, attrs, 0);
    }

    public LogoToolbar(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setup(context, attrs, defStyleAttr);
    }

    private void setup(Context context, AttributeSet attrs, int defStyleAttr) {;
        LayoutInflater.from(context).inflate(R.layout.view_toolbar_logo, this, true);
    }

}
