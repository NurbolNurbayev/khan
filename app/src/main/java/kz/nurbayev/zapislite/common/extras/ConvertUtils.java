package kz.nurbayev.zapislite.common.extras;

import android.content.Context;
import android.util.DisplayMetrics;

public class ConvertUtils {

    public static int millisToSeconds(long millis) {
        return Math.round(millis / 1000.0f);
    }

    public static int secondsToMinutes(long seconds) {
        return Math.round(seconds / 60.0f);
    }

    public static int metersToKilometers(double meters) {
        return (int) Math.round(meters / 1000.0);
    }

    public static float dpToPx(float dp, Context context){
        return dp * ((float) context.getResources().getDisplayMetrics().densityDpi / DisplayMetrics.DENSITY_DEFAULT);
    }

    public static float pxToDp(float px, Context context){
        return px / ((float) context.getResources().getDisplayMetrics().densityDpi / DisplayMetrics.DENSITY_DEFAULT);
    }
}
