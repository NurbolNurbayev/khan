package kz.nurbayev.zapislite.common.mvp;

import android.app.Activity;
import android.content.Context;

public interface Contract {

    interface View {
        Context getApplicationContext();
        Activity getActivity();
        void init();
        void toast(int messageResource);
    }

    interface Presenter {
        void onCreate();
        void onDestroy();
    }

    interface Repository {
        void onDestroy();
    }
}
