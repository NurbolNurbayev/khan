package kz.nurbayev.zapislite.common.network;

import androidx.annotation.Nullable;


public interface VolleyCallback<T> {

    void onSuccess(@Nullable T result);
    void onFailure(@Nullable Integer errorMessageResource);
}
