package kz.nurbayev.zapislite.common.network;

public class NetworkConstants {
    public static final String IMAGE_URL = "http://zp.jgroup.kz/";
    public static final String APP_BASE_URL = "http://zp.jgroup.kz/rest/clients-app/v1/screen/home";
    public static final String APP_GET_DETAILS_URL = "http://zp.jgroup.kz/rest/clients-app/v1/firms/";
    public static final String CONTENT_TYPE_JSON = "application/json; charset=utf-8";
}
