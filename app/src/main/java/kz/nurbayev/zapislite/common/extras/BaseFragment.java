package kz.nurbayev.zapislite.common.extras;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import com.marcinorlowski.fonty.Fonty;

public class BaseFragment extends Fragment {

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Fonty.setFonts((ViewGroup) view);
    }

    @Nullable
    public Context getApplicationContext() {
        if (getActivity() != null) {
            return getActivity().getApplicationContext();

        } else {
            return null;
        }
    }

    public void toast(int messageResource) {
        ToastUtils.showToast(getContext(), messageResource);
    }
}
