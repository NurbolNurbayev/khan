package kz.nurbayev.zapislite;

import android.app.Application;

import com.marcinorlowski.fonty.Fonty;

public class App extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        setupFonts();
    }

    private void setupFonts() {
        Fonty.context(this)
                .normalTypeface(R.string.font_regular_fonty)
                .boldTypeface(R.string.font_bold_fonty)
                .italicTypeface(R.string.font_italic_fonty)
                .build();
    }
}
