package kz.nurbayev.zapislite.content.firms.details;

import java.util.List;

import kz.nurbayev.zapislite.common.mvp.Contract;
import kz.nurbayev.zapislite.common.network.VolleyCallback;
import kz.nurbayev.zapislite.content.firms.details.services.Service;

public interface FirmDetailsContract extends Contract {

    interface View extends Contract.View {
        void setLoading(boolean isLoading);

        void setFirm(FirmDetail firmDetail);

        void setupRecyclerView();

        void setServices(List<Service> services);
    }

    interface Presenter extends Contract.Presenter {
        void getServices();

        void getFirm();
    }

    interface Repository extends Contract.Repository {
        void getServices(long id, VolleyCallback<List<Service>> callback);

        void getFirm(long id, VolleyCallback<FirmDetail> callback);
    }
}
