package kz.nurbayev.zapislite.content.firms.list;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import kz.nurbayev.zapislite.R;
import kz.nurbayev.zapislite.common.extras.BaseFragment;
import kz.nurbayev.zapislite.content.firms.details.FirmDetailsActivity;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class FirmListFragment extends BaseFragment implements FirmListContract.View {

    private FirmListContract.Presenter presenter;
    private View root;
    private ProgressBar progressBar;
    private TextView emptyFirmsTv;
    private RecyclerView firmsRv;
    private FirmListAdapter adapter;

    public static FirmListFragment newInstance() {
        return new FirmListFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_firms, container, false);
        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        presenter = new FirmListPresenter(this);
        presenter.onCreate();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        presenter.onDestroy();
    }

    @Override
    public void init() {
        progressBar = root.findViewById(R.id.progressBar);
        emptyFirmsTv = root.findViewById(R.id.emptyFirmsTv);
        firmsRv = root.findViewById(R.id.firmsRv);
        adapter = new FirmListAdapter(this);
    }

    @Override
    public void setupRecyclerView() {
        firmsRv.setAdapter(adapter);
    }

    @Override
    public void addFirms(List<Firm> firmList) {
        adapter.addFirms(firmList);
    }

    @Override
    public void onFirmClick(Firm firm) {
        Intent startPromotionDetails = new Intent(getContext(), FirmDetailsActivity.class);
        startPromotionDetails.putExtra(FirmDetailsActivity.EXTRA_FIRM_ID, firm.getId());
        startActivity(startPromotionDetails);
    }

    @Override
    public void showProgressBar() {
        emptyFirmsTv.setVisibility(GONE);
        firmsRv.setVisibility(GONE);
        progressBar.setVisibility(VISIBLE);
    }

    @Override
    public void showEmptyContent() {
        firmsRv.setVisibility(GONE);
        progressBar.setVisibility(GONE);
        emptyFirmsTv.setVisibility(VISIBLE);
    }

    @Override
    public void showContent() {
        emptyFirmsTv.setVisibility(GONE);
        progressBar.setVisibility(GONE);
        firmsRv.setVisibility(VISIBLE);
    }

}
