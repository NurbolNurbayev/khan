package kz.nurbayev.zapislite.content.firms.details.services;

import org.json.JSONException;
import org.json.JSONObject;

public class Service {

    private long id;
    private String name;
    private String priceStr;

    public Service(JSONObject serviceJson) throws JSONException {
        this.id = serviceJson.getLong("id");
        this.name = serviceJson.getString("name");
        this.priceStr = serviceJson.getString("priceStr");
    }


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPriceStr() {
        return priceStr;
    }

    public void setPriceStr(String priceStr) {
        this.priceStr = priceStr;
    }
}
