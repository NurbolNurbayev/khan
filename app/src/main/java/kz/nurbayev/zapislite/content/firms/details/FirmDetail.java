package kz.nurbayev.zapislite.content.firms.details;

import org.json.JSONException;
import org.json.JSONObject;

import static kz.nurbayev.zapislite.common.network.NetworkConstants.IMAGE_URL;

public class FirmDetail {

    private long id;
    private String name;
    private String address;
    private String description;
    private String workSchedule;
    private String avatar;
    private double rating;
    private String sales;
    private String firmType;
    private String phoneNumbers;
    private String insta;

    public FirmDetail(JSONObject firmsJson) throws JSONException {
        this.id = firmsJson.getLong("id");
        this.name = firmsJson.getString("name");
        this.firmType = firmsJson.getString("type");
        this.address = firmsJson.getString("address");
        this.description = firmsJson.getString("description");
        this.rating = firmsJson.getDouble("averageRating");
        this.insta = firmsJson.getString("instagramProfile");
        this.avatar = IMAGE_URL + firmsJson.getString("avatarUrl");
        this.phoneNumbers = firmsJson.getJSONArray("phoneNumbers").getString(0);
    }

    public String getInsta() {
        return insta;
    }

    public void setInsta(String insta) {
        this.insta = insta;
    }

    public String getPhoneNumbers() {
        return phoneNumbers;
    }

    public void setPhoneNumbers(String phoneNumbers) {
        this.phoneNumbers = phoneNumbers;
    }

    public String getFirmType() {
        return firmType;
    }

    public void setFirmType(String firmType) {
        this.firmType = firmType;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getWorkSchedule() {
        return workSchedule;
    }

    public void setWorkSchedule(String workSchedule) {
        this.workSchedule = workSchedule;
    }

    public String getAvatar() {
        return avatar;
    }

    public double getRating() {
        return rating;
    }

    public void setAvatar(String picture) {
        this.avatar = picture;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    public String getSales() {
        return sales;
    }

    public void setSales(String sales) {
        this.sales = sales;
    }

}
