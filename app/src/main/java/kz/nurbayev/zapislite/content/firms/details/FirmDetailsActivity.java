package kz.nurbayev.zapislite.content.firms.details;

import android.os.Bundle;
import android.util.Log;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.model.GlideUrl;
import com.bumptech.glide.load.resource.bitmap.CenterInside;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;

import java.util.List;

import kz.nurbayev.zapislite.R;
import kz.nurbayev.zapislite.common.extras.BaseActivity;
import kz.nurbayev.zapislite.common.extras.ConvertUtils;
import kz.nurbayev.zapislite.common.extras.TextUtils;
import kz.nurbayev.zapislite.content.firms.details.services.Service;
import kz.nurbayev.zapislite.content.firms.details.services.ServiceListAdapter;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class FirmDetailsActivity extends BaseActivity implements FirmDetailsContract.View {

    private FirmDetailsContract.Presenter presenter;
    private ProgressBar progressBar;
    private ViewGroup content;
    private ImageView avatarIv;
    private TextView headerTv;
    private TextView instaTv;
    private TextView phoneTv;
    private TextView contentTv;
    private ServiceListAdapter adapter;
    private RecyclerView servicesRv;

    public static String EXTRA_FIRM_ID = "EXTRA_FIRM_ID";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_firm_details);

        presenter = new FirmDetailsPresenter(this, getIntent().getLongExtra(EXTRA_FIRM_ID, 0));
        presenter.onCreate();
    }

    @Override
    protected void onStart() {
        super.onStart();
        setupBackButton();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.onDestroy();
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    public void init() {
        progressBar = findViewById(R.id.progressBar);
        content = findViewById(R.id.content);
        avatarIv = findViewById(R.id.avatarIv);
        headerTv = findViewById(R.id.headerTv);
        instaTv = findViewById(R.id.instaTv);
        phoneTv = findViewById(R.id.phoneTv);
        contentTv = findViewById(R.id.firmDetailsContentTv);
        servicesRv = findViewById(R.id.servicesRv);
        adapter = new ServiceListAdapter(this);
    }

    @Override
    public void setLoading(boolean isLoading) {
        if (!isLoading) {
            progressBar.setVisibility(GONE);
            content.setVisibility(VISIBLE);

        } else {
            content.setVisibility(GONE);
            progressBar.setVisibility(VISIBLE);
        }
    }

    @Override
    public void setFirm(FirmDetail firmDetail) {
        int posterCornerRadius = (int) ConvertUtils.pxToDp(
                getResources().getDimensionPixelSize(R.dimen.default_padding_small),
                this
        );

        headerTv.setText(firmDetail.getName());
        phoneTv.setText("Телефон: " + firmDetail.getPhoneNumbers());
        instaTv.setText("Instagram: " + firmDetail.getInsta());
        Log.d("getDescription", firmDetail.getDescription().toString());
        if (!contentTv.equals("")) {
            contentTv.setText(TextUtils.textToHtml(firmDetail.getDescription()));
        } else contentTv.setText("Нет описания");
        if (firmDetail.getAvatar() != null) {
            GlideUrl imageUrl = new GlideUrl(firmDetail.getAvatar());
            Glide.with(this)
                    .load(imageUrl)
                    .transform(new CenterInside(), new RoundedCorners(posterCornerRadius))
                    .into(avatarIv);
            avatarIv.setVisibility(VISIBLE);

        } else {
            avatarIv.setVisibility(GONE);
        }
    }

    @Override
    public void setupRecyclerView() {
        servicesRv.setAdapter(adapter);
    }

    @Override
    public void setServices(List<Service> services) {
        adapter.addService(services);
    }

}
