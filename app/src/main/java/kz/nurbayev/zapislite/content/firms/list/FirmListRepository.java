package kz.nurbayev.zapislite.content.firms.list;

import android.content.Context;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import kz.nurbayev.zapislite.common.network.Utf8StringRequest;
import kz.nurbayev.zapislite.common.network.VolleyCallback;
import kz.nurbayev.zapislite.common.network.VolleyClient;

import static kz.nurbayev.zapislite.common.network.NetworkConstants.APP_BASE_URL;


class FirmListRepository implements FirmListContract.Repository {

    private static final String TAG = "FirmListRepository";

    private RequestQueue requestQueue;

    FirmListRepository(Context context) {
        requestQueue = VolleyClient.getInstance(context).getRequestQueue();
    }

    @Override
    public void onDestroy() {
        requestQueue.cancelAll(TAG);
    }


    @Override
    public void getFirms(VolleyCallback<List<Firm>> callback) {
        String getFirmsUrl = APP_BASE_URL;
        Utf8StringRequest getFirmsRequest = new Utf8StringRequest(
                Request.Method.GET,
                getFirmsUrl,
                response -> {
                    try {
                        JSONObject responseJson = new JSONObject(response);
                        JSONObject dataJson = responseJson.getJSONObject("data");
                        JSONArray firmJson = dataJson.getJSONArray("recommendedFirms");
                        List<Firm> firmList = new ArrayList<>();
                        for (int i = 0; i < firmJson.length(); i++) {
                            firmList.add(new Firm(firmJson.getJSONObject(i)));
                        }
                        callback.onSuccess(firmList);

                    } catch (JSONException e) {
                        callback.onFailure(null);
                    }
                },
                error -> {
                    callback.onFailure(null);
                }
        );

        requestQueue.add(getFirmsRequest.setTag(TAG));

    }
}
