package kz.nurbayev.zapislite.content.firms.details;

import android.widget.Toast;

import androidx.annotation.Nullable;

import java.util.List;

import kz.nurbayev.zapislite.common.network.VolleyCallback;
import kz.nurbayev.zapislite.content.firms.details.services.Service;


class FirmDetailsPresenter implements FirmDetailsContract.Presenter {

    private FirmDetailsContract.View view;
    private FirmDetailsContract.Repository repository;
    private long firmId;

    FirmDetailsPresenter(FirmDetailsContract.View view, long firmId) {
        this.view = view;
        this.repository = new FirmDetailsRepository(view.getApplicationContext());
        this.firmId = firmId;
    }

    @Override
    public void onCreate() {
        view.init();
        getFirm();
        getServices();
        view.setupRecyclerView();
    }

    @Override
    public void onDestroy() {
        repository.onDestroy();
        view = null;
        repository = null;
    }


    @Override
    public void getServices() {
        view.setLoading(true);
        repository.getServices(firmId, new VolleyCallback<List<Service>>() {
            @Override
            public void onSuccess(@Nullable List<Service> result) {
                view.setServices(result);
                view.setLoading(false);
            }

            @Override
            public void onFailure(@Nullable Integer errorMessageResource) {
                Toast.makeText(view.getApplicationContext(), "Произошла ошибка", Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public void getFirm() {
        view.setLoading(true);
        repository.getFirm(firmId, new VolleyCallback<FirmDetail>() {
            @Override
            public void onSuccess(@Nullable FirmDetail result) {
                view.setFirm(result);
                view.setLoading(false);
            }

            @Override
            public void onFailure(@Nullable Integer errorMessageResource) {
                Toast.makeText(view.getApplicationContext(), "Произошла ошибка", Toast.LENGTH_LONG).show();
            }
        });
    }
}
