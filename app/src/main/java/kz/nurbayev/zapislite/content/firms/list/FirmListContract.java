package kz.nurbayev.zapislite.content.firms.list;

import java.util.List;

import kz.nurbayev.zapislite.common.mvp.Contract;
import kz.nurbayev.zapislite.common.network.VolleyCallback;

interface FirmListContract extends Contract {

    interface View extends Contract.View {
        void showProgressBar();

        void showEmptyContent();

        void showContent();

        void setupRecyclerView();

        void addFirms(List<Firm> firmList);

        void onFirmClick(Firm firm);
    }

    interface Presenter extends Contract.Presenter {
        void getFirms();
    }

    interface Repository extends Contract.Repository {
        void getFirms(VolleyCallback<List<Firm>> callback);
    }
}
