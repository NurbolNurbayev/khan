package kz.nurbayev.zapislite.content.firms.list;

import org.json.JSONException;
import org.json.JSONObject;

import static kz.nurbayev.zapislite.common.network.NetworkConstants.IMAGE_URL;

public class Firm {

    private long id;
    private String name;
    private String address;
    private String avatar;
    private String workSchedule;
    private String picture;
    private double rating;
    private String sales;
    private String firmType;

    public Firm(JSONObject firmsJson) throws JSONException {
        this.id = firmsJson.getLong("id");
        this.name = firmsJson.getString("name");
        this.firmType = firmsJson.getString("type");
        this.address = firmsJson.getString("address");
        this.avatar = IMAGE_URL + firmsJson.getString("avatarUrl");
        this.workSchedule = firmsJson.getString("workSchedule");
        this.rating = firmsJson.getDouble("averageRating");
        this.picture = IMAGE_URL + firmsJson.getString("pictureUrl");
    }

    public String getFirmType() {
        return firmType;
    }

    public void setFirmType(String firmType) {
        this.firmType = firmType;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getWorkSchedule() {
        return workSchedule;
    }

    public void setWorkSchedule(String workSchedule) {
        this.workSchedule = workSchedule;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    public String getSales() {
        return sales;
    }

    public void setSales(String sales) {
        this.sales = sales;
    }


}
