package kz.nurbayev.zapislite.content.firms.details;

import android.content.Context;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import kz.nurbayev.zapislite.common.network.Utf8StringRequest;
import kz.nurbayev.zapislite.common.network.VolleyCallback;
import kz.nurbayev.zapislite.common.network.VolleyClient;
import kz.nurbayev.zapislite.content.firms.details.services.Service;

import static kz.nurbayev.zapislite.common.network.NetworkConstants.APP_GET_DETAILS_URL;


class FirmDetailsRepository implements FirmDetailsContract.Repository {
    private static final String TAG = "FirmDetailsRepository";
    private RequestQueue requestQueue;

    FirmDetailsRepository(Context context) {
        requestQueue = VolleyClient.getInstance(context).getRequestQueue();
    }

    @Override
    public void onDestroy() {
        requestQueue.cancelAll(TAG);
    }

    @Override
    public void getServices(long id, VolleyCallback<List<Service>> callback) {
        String getServicesUrl = APP_GET_DETAILS_URL + id;
        Utf8StringRequest getServicesRequest = new Utf8StringRequest(
                Request.Method.GET,
                getServicesUrl,
                response -> {
                    try {
                        JSONObject responseJson = new JSONObject(response);
                        JSONObject dataJson = responseJson.getJSONObject("data");
                        JSONObject firm = dataJson.getJSONObject("firm");
                        JSONArray services = dataJson.getJSONArray("services");
                        List<Service> serviceList = new ArrayList<>();
                        for (int i = 0; i < services.length(); i++) {
                            serviceList.add(new Service(services.getJSONObject(i)));
                        }
                        callback.onSuccess(serviceList);

                    } catch (JSONException e) {
                        callback.onFailure(null);
                    }
                },
                error -> {
                    callback.onFailure(null);
                }
        );

        requestQueue.add(getServicesRequest.setTag(TAG));
    }

    @Override
    public void getFirm(long id, VolleyCallback<FirmDetail> callback) {
        String getFirmDetailUrl = APP_GET_DETAILS_URL + id;
        Utf8StringRequest getFirmDetailRequest = new Utf8StringRequest(
                Request.Method.GET,
                getFirmDetailUrl,
                response -> {
                    try {
                        JSONObject responseJson = new JSONObject(response);
                        JSONObject dataJson = responseJson.getJSONObject("data");
                        JSONObject firm = dataJson.getJSONObject("firm");
                        FirmDetail firmDetail = new FirmDetail(firm);
                        callback.onSuccess(firmDetail);

                    } catch (JSONException e) {
                        callback.onFailure(null);
                    }
                },
                error -> {
                    callback.onFailure(null);
                }
        );

        requestQueue.add(getFirmDetailRequest.setTag(TAG));
    }
}
