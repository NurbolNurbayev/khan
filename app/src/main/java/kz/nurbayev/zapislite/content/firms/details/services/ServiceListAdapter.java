package kz.nurbayev.zapislite.content.firms.details.services;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.marcinorlowski.fonty.Fonty;

import java.util.ArrayList;
import java.util.List;

import kz.nurbayev.zapislite.R;
import kz.nurbayev.zapislite.content.firms.details.FirmDetailsContract;

public class ServiceListAdapter extends RecyclerView.Adapter<ServiceListAdapter.ServiceHolder> {

    private View root;
    private List<Service> serviceList;
    private FirmDetailsContract.View callback;

    public ServiceListAdapter(FirmDetailsContract.View callback) {
        this.callback = callback;
    }

    public void addService(List<Service> services) {
        if (this.serviceList == null) {
            this.serviceList = new ArrayList<>();
        }
        this.serviceList.addAll(services);
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return serviceList != null ? serviceList.size() : 0;
    }

    @NonNull
    @Override
    public ServiceHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View root = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_services, parent, false);
        Fonty.setFonts((ViewGroup) root);
        return new ServiceHolder(root);
    }

    @Override
    public void onBindViewHolder(@NonNull ServiceHolder holder, int position) {
        holder.bind(serviceList.get(position), callback);
    }

    static class ServiceHolder extends RecyclerView.ViewHolder {
        private View root;
        private TextView serviceNameTv;
        private TextView servicePriceTv;

        private static int posterCornerRadius = Integer.MIN_VALUE;

        ServiceHolder(@NonNull View root) {
            super(root);
            this.root = root;
            serviceNameTv = root.findViewById(R.id.serviceNameTv);
            servicePriceTv = root.findViewById(R.id.servicePriceTv);
        }

        void bind(Service service, FirmDetailsContract.View callback) {
            serviceNameTv.setText(service.getName());
            servicePriceTv.setText(service.getPriceStr());
        }
    }
}
