package kz.nurbayev.zapislite.content.firms.list;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.model.GlideUrl;
import com.bumptech.glide.load.model.LazyHeaders;
import com.bumptech.glide.load.resource.bitmap.CenterInside;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.marcinorlowski.fonty.Fonty;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import kz.nurbayev.zapislite.R;
import kz.nurbayev.zapislite.common.extras.ConvertUtils;
import kz.nurbayev.zapislite.common.extras.TextUtils;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

class FirmListAdapter extends RecyclerView.Adapter<FirmListAdapter.FirmHolder> {
    private List<Firm> firmList;
    private FirmListContract.View callback;

    FirmListAdapter(FirmListContract.View callback) {
        this.callback = callback;
    }

    void addFirms(List<Firm> firms) {
        if (this.firmList == null) {
            this.firmList = new ArrayList<>();
        }
        this.firmList.addAll(firms);
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return firmList != null ? firmList.size() : 0;
    }

    @NonNull
    @Override
    public FirmHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View root = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_firm_list, parent, false);
        Fonty.setFonts((ViewGroup) root);
        return new FirmHolder(root);
    }

    @Override
    public void onBindViewHolder(@NonNull FirmHolder holder, int position) {
        holder.bind(firmList.get(position), callback);
    }

    static class FirmHolder extends RecyclerView.ViewHolder {

        private View root;
        private ImageView posterIv;
        private RatingBar ratingBar;
        private TextView titleTv;
        private TextView firmType;
        private TextView averageRatingTv;
        private TextView addressTv;

        private static int posterCornerRadius = Integer.MIN_VALUE;

        FirmHolder(@NonNull View root) {
            super(root);
            this.root = root;
            posterIv = root.findViewById(R.id.posterIv);
            titleTv = root.findViewById(R.id.titleTv);
            firmType = root.findViewById(R.id.firmType);
            averageRatingTv = root.findViewById(R.id.averageRatingTv);
            addressTv = root.findViewById(R.id.addressTv);
            ratingBar = root.findViewById(R.id.ratingBar);

            if (posterCornerRadius == Integer.MIN_VALUE) {
                posterCornerRadius = (int) ConvertUtils.pxToDp(
                        root.getResources().getDimensionPixelSize(R.dimen.default_padding_small),
                        root.getContext()
                );
            }
        }

        void bind(Firm firm, FirmListContract.View callback) {
            titleTv.setText(firm.getName());
            firmType.setText(firm.getFirmType());
            averageRatingTv.setText(String.valueOf(firm.getRating()));
            addressTv.setText(TextUtils.textToHtml(firm.getAddress()));
            ratingBar.setRating((float) firm.getRating());
            if (firm.getAvatar() != null) {
                GlideUrl imageUrl = new GlideUrl(firm.getAvatar());
                Glide.with(root)

                        .load(imageUrl)
                        .transform(new CenterInside(), new RoundedCorners(posterCornerRadius))
                        .into(posterIv);
                posterIv.setVisibility(VISIBLE);
            } else {
                posterIv.setVisibility(GONE);
            }

            root.setOnClickListener(v -> callback.onFirmClick(firm));
        }
    }
}
