package kz.nurbayev.zapislite.content.firms.list;

import android.widget.Toast;

import androidx.annotation.Nullable;

import java.util.List;

import kz.nurbayev.zapislite.common.network.VolleyCallback;

class FirmListPresenter implements FirmListContract.Presenter {


    private FirmListContract.View view;
    private FirmListContract.Repository repository;

    FirmListPresenter(FirmListContract.View view) {
        this.view = view;
        this.repository = new FirmListRepository(view.getApplicationContext());
    }

    @Override
    public void onCreate() {
        view.init();
        view.setupRecyclerView();
        getFirms();
    }

    @Override
    public void onDestroy() {
        repository.onDestroy();
        view = null;
        repository = null;
    }


    @Override
    public void getFirms() {
        repository.getFirms(new VolleyCallback<List<Firm>>() {
            @Override
            public void onSuccess(@Nullable List<Firm> result) {
                if (result != null) {
                    view.showProgressBar();
                    view.addFirms(result);
                    view.showContent();
                } else {
                    view.showEmptyContent();
                }
            }

            @Override
            public void onFailure(@Nullable Integer errorMessageResource) {
                Toast.makeText(view.getApplicationContext(), "Произошла ошибка", Toast.LENGTH_LONG).show();
            }
        });

    }
}
